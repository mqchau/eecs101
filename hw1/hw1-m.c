#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define ROWS   		 		 480
#define COLS				 640
#define LOGICAL_X_MIN			-4.0
#define LOGICAL_X_MAX			 4.0
#define LOGICAL_Y_MIN			-4.0
#define LOGICAL_Y_MAX			 4.0

void clear( unsigned char image[][COLS] );
int plot_logical_point( float x, float y, unsigned char image[][COLS] );
int plot_physical_point( int x, int y, unsigned char image[][COLS] );
int in_range( int x, int y );
void header( int row, int col, unsigned char head[32] );

void save_ras(char * file_name, unsigned char	image[ROWS][COLS]){
	FILE			*fp;
	unsigned char head[32];
	int i;
	
	/* Create a header */
	header(ROWS, COLS, head);

	/* Save it into a ras image */
	/* Open the file */
	if (!( fp = fopen( file_name, "wb" )))
	  fprintf( stderr, "error: could not open %s\n", file_name ), exit(1);

	/* Write the header */
	fwrite( head, 4, 8, fp );
	 
	/* Write the image */
	for ( i = 0; i < ROWS; i++ ) 
		fwrite( image[i], 1, COLS, fp );

	/* Close the file */
	fclose( fp );
}

void getImageQuestion3(int b, int c){
	char tempbuff[50];
	unsigned char	image[ROWS][COLS];
	float			t, x, y;
	
	/*perspective*/
	for ( t = 0.01; t < 50.0; t += .01 )
	{
	  x = -1 / (t * c);
	  y = (-1 + t*b)/(t*c);
	 
	  plot_logical_point( x, y, image );
	  
	  x = 1/(t*c);
	  x = 1/(t*c);
	  y = (-1+t*b)/(t*c);
	 
	  plot_logical_point( x, y, image );
	}	
	sprintf(tempbuff, "quest3-perspective_b%d_c%d.ras", b, c);
	save_ras(tempbuff, image);
	clear(image);
	
	/*orthographic*/
	for ( t = 0.01; t < 1000.0; t += .01 )
	{
	  x = -1;
	  y = -1 + t * b;
	 
	  plot_logical_point( x, y, image );
	  
	  x = 1;
	  y = -1 + t * b;
	 
	  plot_logical_point( x, y, image );
	}	
	sprintf(tempbuff, "quest3-orthographic_b%d_c%d.ras", b, c);
	save_ras(tempbuff, image);
	clear(image);
}

int main( int argc, char **argv )
{
	
	float			t, x, y, z, xprime, yprime;
	
	unsigned char	image[ROWS][COLS];

	
	/* Clear image buffer */
	clear(image);

	/*for question 1*/
	/*original*/	
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  /* Modify these according to the parametric equations */
	  x = 0.5 + t * 0.0;
	  y = -1.0 + t;
	  z = -t;

	  /* Modify these according to the projection */
	  xprime = x;			
	  yprime = y;			

	  plot_logical_point( xprime, yprime, image );
	}	
	save_ras("quest1-orig.ras", image);
	clear(image);
	
	/*perspective*/	
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  /* Modify these according to the parametric equations */
	  x = -0.5 / t ;
	  y = (1 - t) / t;
	  /* Modify these according to the projection */
	  xprime = x;			
	  yprime = y;			

	  plot_logical_point( xprime, yprime, image );
	}	
	save_ras("quest1-perspective.ras", image);
	clear(image);
	
	/*orthographic*/	
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  /* Modify these according to the parametric equations */
	  x = 0.5;
	  y = (1 + t);
	  /* Modify these according to the projection */
	  xprime = x;			
	  yprime = y;			

	  plot_logical_point( xprime, yprime, image );
	}	
	save_ras("quest1-orthographic.ras", image);
	clear(image);
	
	/*==========Question 2, z = -1==========*/
	z = -1;
	/*perspective*/
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  x = (0.5 + t) / z;
	  y = (t-1) / z;
	 
	  plot_logical_point( x, y, image );
	  
	  x = (-0.5 + t) / z;
	  y = (t - 1) / z;
	 
	  plot_logical_point( x, y, image );
	}	
	save_ras("quest2-perspective-1.ras", image);
	clear(image);
	
	/*orthographic*/
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  x = (0.5 + t) ;
	  y = (t - 1);
	 
	  plot_logical_point( x, y, image );
	  
	  x = (-0.5 + t);
	  y = (t -1);
	 
	  plot_logical_point( x, y, image );
	}	
	save_ras("quest2-orthographic-1.ras", image);
	clear(image);
	
	/*==========Question 2, z = -2==========*/
	z = -2;
	/*perspective*/
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  x = (0.5 + t) / z;
	  y = (t-1) / z;
	 
	  plot_logical_point( x, y, image );
	  
	  x = (-0.5 + t) / z;
	  y = (t - 1) / z;
	 
	  plot_logical_point( x, y, image );
	}	
	save_ras("quest2-perspective-2.ras", image);
	clear(image);
	
	/*orthographic*/
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  x = (0.5 + t) ;
	  y = (t - 1);
	 
	  plot_logical_point( x, y, image );
	  
	  x = (-0.5 + t);
	  y = (t -1);
	 
	  plot_logical_point( x, y, image );
	}	
	save_ras("quest2-orthographic-2.ras", image);
	clear(image);
	
	/*==========Question 2, z = -3==========*/
	z = -3;
	/*perspective*/
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  x = (0.5 + t) / z;
	  y = (t-1) / z;
	 
	  plot_logical_point( x, y, image );
	  
	  x = (-0.5 + t) / z;
	  y = (t - 1) / z;
	 
	  plot_logical_point( x, y, image );
	}	
	save_ras("quest2-perspective-3.ras", image);
	clear(image);
	
	/*orthographic*/
	for ( t = 0.01; t < 3.9; t += .01 )
	{
	  x = (0.5 + t) ;
	  y = (t - 1);
	 
	  plot_logical_point( x, y, image );
	  
	  x = (-0.5 + t);
	  y = (t -1);
	 
	  plot_logical_point( x, y, image );
	}	
	save_ras("quest2-orthographic-3.ras", image);
	clear(image);
	
	/*==========Question 3, b = -1, c = -1==========*/
	int b,c;
	
	for (b = -1; b <= 1; b++){
		c = -1;
		getImageQuestion3(b,c);
		c = 1;
		getImageQuestion3(b,c);
	}
	
	return 0;
}

void clear( unsigned char image[][COLS] )
{
	int	i,j;
	for ( i = 0 ; i < ROWS ; i++ )
		for ( j = 0 ; j < COLS ; j++ ) image[i][j] = 0;
}

int plot_logical_point( float x, float y, unsigned char image[][COLS] )
{
	int	nx, ny;
	float	xc, yc;
	xc = COLS / ((float)LOGICAL_X_MAX - LOGICAL_X_MIN);
	yc = ROWS / ((float)LOGICAL_Y_MAX - LOGICAL_Y_MIN);
	nx = (x - LOGICAL_X_MIN) * xc;
	ny = (y - LOGICAL_Y_MIN) * yc;
	return plot_physical_point( nx, ny, image );
}

int plot_physical_point( int x, int y, unsigned char image[][COLS] )
{
	if (! in_range(x,y) ) return 0;
	return image[y][x] = 255;
}
	int in_range( int x, int y )
	{
		return x >= 0 && x < COLS && y >= 0 && y < ROWS;
	}

void header( int row, int col, unsigned char head[32] )
{
	int *p = (int *)head;
	char *ch;
	int num = row * col;

	/* Choose little-endian or big-endian header depending on the machine. Don't modify this */
	/* Little-endian for PC */
	
	*p = 0x956aa659;
	*(p + 3) = 0x08000000;
	*(p + 5) = 0x01000000;
	*(p + 6) = 0x0;
	*(p + 7) = 0xf8000000;

	ch = (char*)&col;
	head[7] = *ch;
	ch ++; 
	head[6] = *ch;
	ch ++;
	head[5] = *ch;
	ch ++;
	head[4] = *ch;

	ch = (char*)&row;
	head[11] = *ch;
	ch ++; 
	head[10] = *ch;
	ch ++;
	head[9] = *ch;
	ch ++;
	head[8] = *ch;
	
	ch = (char*)&num;
	head[19] = *ch;
	ch ++; 
	head[18] = *ch;
	ch ++;
	head[17] = *ch;
	ch ++;
	head[16] = *ch;
	
/*
	// Big-endian for unix
	*p = 0x59a66a95;
	*(p + 1) = col;
	*(p + 2) = row;
	*(p + 3) = 0x8;
	*(p + 4) = num;
	*(p + 5) = 0x1;
	*(p + 6) = 0x0;
	*(p + 7) = 0xf8;
*/
}
