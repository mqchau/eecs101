/****************
 * Name: Quan Chau
 * Student ID: 18831829
 * EECS 101 homework 7 
 ****************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define IMG_NUM_ROWS   		 		 300
#define IMG_NUM_COLS				 300

void saveImage(char *file_name, unsigned char image[IMG_NUM_ROWS][IMG_NUM_COLS]);
void header( int row, int col, unsigned char head[32] );
void Clear( unsigned char image[][IMG_NUM_COLS] );
void constructImage(unsigned char image[IMG_NUM_ROWS][IMG_NUM_COLS], float r, float a, float m, float v[3], float s[3]);

int main( int argc, char **argv )
{

    float a, m, r;
    float v[3];
    float s[3];
    v[0] = 0;
    v[1] = 0;
    v[2] = 1;

    unsigned char	image[IMG_NUM_ROWS][IMG_NUM_COLS];

   /*PART A*/
    r = 50;
    a = 0.5;
    m = 1;
    s[0] = 0;
    s[1] = 0;
    s[2] = 1;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imagea.ras", image);
    
    /*PART B*/
    r = 50;
    a = 0.5;
    m = 1;
    s[0] = 1/sqrt(3);
    s[1] = 1/sqrt(3);
    s[2] = 1/sqrt(3);
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imageb.ras", image);
    
    /*PART C*/
    r = 50;
    a = 0.5;
    m = 1;
    s[0] = 1;
    s[1] = 0;
    s[2] = 0;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imagec.ras", image);
    
    /*PART D*/
    r = 10;
    a = 0.5;
    m = 1;
    s[0] = 0;
    s[1] = 0;
    s[2] = 1;
    r = 10;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imaged.ras", image);
    
    /*PART E*/
    r = 100;
    a = 0.5;
    m = 1;
    s[0] = 0;
    s[1] = 0;
    s[2] = 1;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imagee.ras", image);
    
    /*PART F*/
    r = 50;
    a = 0.1;
    m = 1;
    s[0] = 0;
    s[1] = 0;
    s[2] = 1;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imagef.ras", image);

    /*PART G*/
    r = 50;
    a = 1;
    m = 1;
    s[0] = 0;
    s[1] = 0;
    s[2] = 1;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imageg.ras", image);
    
    /*PART H*/
    r = 50;
    a = 0.5;
    m = 0.1;
    s[0] = 0;
    s[1] = 0;
    s[2] = 1;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imageh.ras", image);

    
    /*PART I*/
    r = 50;
    a = 0.5;
    m = 10000;
    s[0] = 0;
    s[1] = 0;
    s[2] = 1;
    Clear(image);
    constructImage(image, r, a, m, v, s);
    saveImage("imagei.ras", image);

    return 0;
}

void Clear( unsigned char image[][IMG_NUM_COLS] )
{
    int	i,j;
    for ( i = 0 ; i < IMG_NUM_ROWS ; i++ )
        for ( j = 0 ; j < IMG_NUM_COLS ; j++ ) image[i][j] = 0;
}

void saveImage(char *file_name, unsigned char image[IMG_NUM_ROWS][IMG_NUM_COLS]){
    int				i;
    FILE			*fp;
    unsigned char head[32];
    


    header(IMG_NUM_ROWS, IMG_NUM_COLS, head);
    
    /* Open the file */
    if (!( fp = fopen(file_name, "wb" )))
        fprintf(stderr, "error: could not open %s\n", file_name), exit(1);

    /* Write the header */
    fwrite(head, 4, 8, fp );

    /* Write the image */
    for (i = 0; i < IMG_NUM_ROWS; i++)
        fwrite( image[i], 1, IMG_NUM_COLS, fp);

    /* Close the file */
    fclose( fp );
}

void constructImage(unsigned char image[IMG_NUM_ROWS][IMG_NUM_COLS], float r, float a, float m, float v[3], float s[3])
{
    int x,y;
    int tempX, tempY;
    float p, q, alpha, ll, ls, l, normalDenom, hDenominator;
    float h[3];

    h[0] = s[0] + v[0];
    h[1] = s[1] + v[1];
    h[2] = s[2] + v[2];
    hDenominator = sqrt(h[0] * h[0] + h[1] * h[1] + h[2] * h[2]);
    h[0] = h[0] / hDenominator;
    h[1] = h[1] / hDenominator;
    h[2] = h[2] / hDenominator;

    /*go through every points on the image*/
    for(y = 0; y < IMG_NUM_ROWS; y++)
    {
        for(x = 0; x < IMG_NUM_COLS; x++)
        {
            tempX = x - IMG_NUM_COLS/2;
            tempY = y - IMG_NUM_ROWS/2;

            /*if the point is inside the circle*/
            if(tempX * tempX + tempY * tempY <= r * r)
            {
                /*calculate the intensity of the pixel*/
                p = -1 * tempX / sqrt(r * r - (tempX * tempX + tempY * tempY));
                q = -1 * tempY / sqrt(r * r - (tempX*  tempX + tempY * tempY));
                normalDenom = sqrt(p * p + q * q + 1);
                ll = (s[0] * -p + s[1] * -q + 1 * s[2]) / normalDenom;
                alpha = acos((h[0] * -p + h[1] * -q + 1 * h[2]) / normalDenom);
                ls = exp(-pow(alpha / m, 2));
                l = a * ll + (1 - a) * ls;
                
                /*assign the intensity to the pixel*/
				if ( l >= 0) 
                    image[y][x] = (unsigned char)(l * 255); 
            }
        }
    }
}

void header( int row, int col, unsigned char head[32] )
{
    int *p = (int *)head;
    char *ch;
    int num = row * col;
    
    /* Choose little-endian or big-endian header depending on the machine. Don't modify this */
    /* Little-endian for PC */
    
    *p = 0x956aa659;
    *(p + 3) = 0x08000000;
    *(p + 5) = 0x01000000;
    *(p + 6) = 0x0;
    *(p + 7) = 0xf8000000;
    
    ch = (char*)&col;
    head[7] = *ch;
    ch ++; 
    head[6] = *ch;
    ch ++;
    head[5] = *ch;
    ch ++;
    head[4] = *ch;
    
    ch = (char*)&row;
    head[11] = *ch;
    ch ++; 
    head[10] = *ch;
    ch ++;
    head[9] = *ch;
    ch ++;
    head[8] = *ch;
    
    ch = (char*)&num;
    head[19] = *ch;
    ch ++; 
    head[18] = *ch;
    ch ++;
    head[17] = *ch;
    ch ++;
    head[16] = *ch;
    
    /*
     // Big-endian for unix
     *p = 0x59a66a95;
     *(p + 1) = col;
     *(p + 2) = row;
     *(p + 3) = 0x8;
     *(p + 4) = num;
     *(p + 5) = 0x1;
     *(p + 6) = 0x0;
     *(p + 7) = 0xf8;
     */
}

