/*************************************************
 * EECS 101 Homework 4 
 * Name: Quan Chau
 * Student ID: 18831829
 *************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/*define number of NUM_IMG_ROWS and columns in image*/
#define NUM_IMG_ROWS   		 		480 
#define NUM_IMG_COLS				640 


void header( int row, int col, unsigned char head[32] )
{
	int *p = (int *)head;
	char *ch;
	int num = row * col;

	/* Choose little-endian or big-endian header depending on the machine. Don't modify this */
	/* Little-endian for PC */
	
	*p = 0x956aa659;
	*(p + 3) = 0x08000000;
	*(p + 5) = 0x01000000;
	*(p + 6) = 0x0;
	*(p + 7) = 0xf8000000;

	ch = (char*)&col;
	head[7] = *ch;
	ch ++; 
	head[6] = *ch;
	ch ++;
	head[5] = *ch;
	ch ++;
	head[4] = *ch;

	ch = (char*)&row;
	head[11] = *ch;
	ch ++; 
	head[10] = *ch;
	ch ++;
	head[9] = *ch;
	ch ++;
	head[8] = *ch;
	
	ch = (char*)&num;
	head[19] = *ch;
	ch ++; 
	head[18] = *ch;
	ch ++;
	head[17] = *ch;
	ch ++;
	head[16] = *ch;
	
/*
	// Big-endian for unix
	*p = 0x59a66a95;
	*(p + 1) = col;
	*(p + 2) = row;
	*(p + 3) = 0x8;
	*(p + 4) = num;
	*(p + 5) = 0x1;
	*(p + 6) = 0x0;
	*(p + 7) = 0xf8;
*/
}

void clear( unsigned char image[NUM_IMG_ROWS][NUM_IMG_COLS] )
{
	int	i,j;
	for ( i = 0 ; i < NUM_IMG_ROWS ; i++ )
		for ( j = 0 ; j < NUM_IMG_COLS ; j++ ) image[i][j] = 0;
}

int main(void){
	/*read in the image*/

	/*apply edge detection on x direction*/

	/*apply edge detection on y direction*/

	/*proportion to square gradient*/

	/*generete isolated binary image*/
}

