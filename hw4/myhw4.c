#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROWS   		 		 480
#define COLS				 640

void header(int row, int col, unsigned char head[32])
{
	
   int *p = (int *)head;
    char *ch;
    int num = row * col;
    
    /* Choose little-endian or big-endian header depending on the machine. Don't modify this */
    /* Little-endian for PC */
    
    *p = 0x956aa659;
    *(p + 3) = 0x08000000;
    *(p + 5) = 0x01000000;
    *(p + 6) = 0x0;
    *(p + 7) = 0xf8000000;
    
    ch = (char*)&col;
    head[7] = *ch;
    ch ++; 
    head[6] = *ch;
    ch ++;
    head[5] = *ch;
    ch ++;
    head[4] = *ch;
    
    ch = (char*)&row;
    head[11] = *ch;
    ch ++; 
    head[10] = *ch;
    ch ++;
    head[9] = *ch;
    ch ++;
    head[8] = *ch;
    
    ch = (char*)&num;
    head[19] = *ch;
    ch ++; 
    head[18] = *ch;
    ch ++;
    head[17] = *ch;
    ch ++;
    head[16] = *ch;
    
    /*
     // Big-endian for unix
     *p = 0x59a66a95;

     *(p + 1) = col;
     *(p + 2) = row;
     *(p + 3) = 0x8;
     *(p + 4) = num;
     *(p + 5) = 0x1;

     *(p + 6) = 0x0;
     *(p + 7) = 0xf8;
     */
}

void read_image(char ifile[20], unsigned char	image[ROWS][COLS])
{
	int k; FILE * fp;
	if((fp = fopen( ifile, "rb")) == NULL)
	{
		fprintf( stderr, "error: couldn't open %s\n", ifile );
		exit( 1 );
	}
	
	/* Read the file */
	for (k = 0; k < ROWS ; k++)
	{
		if (fread( image[k], 1, COLS, fp ) != COLS)
		{
			fprintf( stderr, "error: couldn't read enough stuff\n" );
			exit( 1 );
		}
	}
	
	/* Close the file */
	fclose( fp );
}

void Clear( unsigned char image[ROWS][COLS])
{
    int	i,j;
    for ( i = 0 ; i < ROWS ; i++ )
        for ( j = 0 ; j < COLS ; j++ ) image[i][j] = 0;
}

void write_image(char ofile[20], unsigned char	image[ROWS][COLS])
{
	int k;
	unsigned char head[32];
	header(ROWS, COLS, head);
	FILE * fp;			
	if (!(fp = fopen( ofile, "wb")))
		fprintf( stderr, "error: could not open %s\n", ofile ), exit(1);
	
	fwrite( head, 4, 8, fp );
	
	for (k = 0; k < ROWS; k++)
		fwrite(image[k], 1, COLS, fp);
	
	fclose(fp);
}

void x_sobel(unsigned char	image[ROWS][COLS], int imageDx[ROWS][COLS])
{
	int sobelDx_matrix[3][3] = {{ -1,  0,  1 },
                    { -2,  0,  2 },
                    { -1,  0,  1 }};
	int first_time = 0;

	int y, x, k, xprime, max, l;
		
	for(y = 1; y < ROWS - 3; y++)
	{
		for(x = 1; x < COLS - 3; x++)
		{
			xprime = 0;
			for(l = 0; l < 3; l++)
				for(k = 0; k < 3; k++)
					xprime += image[k + y][l + x] * sobelDx_matrix[k][l];
			if(xprime < 0)
				xprime *= -1;
			if(xprime > max || first_time == 0){
				first_time = 1;
				max = xprime;
			}
			imageDx[y][x] = xprime;
		}
	}
	
	printf("Max on dx: %f\n", (float)max);
	for(y = 0; y < ROWS; y++)
	{
		for( x = 0; x < COLS; x ++){
			image[y][x] = (float)imageDx[y][x]/(float)max * 255.0;
		}
	}
}

void y_sobel(unsigned char	image[ROWS][COLS], int imageDy[ROWS][COLS])
{	
	int sobelDy_matrix[3][3] = {{ 1,  2,  1 },
                    { 0,  0,  0 },
                    { -1,  -2,  -1 }};
	int first_time = 0;
	int y, k,x, l, yprime, max;

	for(y = 1; y < ROWS - 3; y++)
	{
		for(x = 1; x < COLS - 3; x++)
		{
			yprime = 0;
			for(l = 0; l < 3; l++)
				for(k = 0; k < 3; k++)
					yprime += image[k + y][l + x] * sobelDy_matrix[k][l];
			if(yprime < 0)
				yprime *= -1;
			if(yprime > max || first_time == 0){
				max = yprime;
				first_time = 1;
			}
			imageDy[y][x] = yprime;
		}
	}
	
	printf("Max on dy: %f\n", (float)max);
	for(y = 0; y < ROWS; y++)
	{
		for( x = 0; x < COLS; x ++)
			image[y][x] = (float)imageDy[y][x]/(float)max * 255;
	}
}

void sgm(unsigned char	image[ROWS][COLS], int imageDx[ROWS][COLS], int imageDy[ROWS][COLS], unsigned char imageSgm[ROWS][COLS])
{
	int tempSgm[ROWS][COLS];
	int maxsgm = 0, y, x; int first_time = 0;
	for(y = 0; y < ROWS ; y++)
	{
		for(x = 0; x < COLS ; x++)
		{
			tempSgm[y][x] = ((int)imageDx[y][x]*imageDx[y][x])+((int)imageDy[y][x]*imageDy[y][x]);
			if(tempSgm[y][x] > maxsgm || first_time == 0){
				printf("%d %d %d %d\n", y, x, imageDx[y][x], imageDy[y][x]);
				first_time = 1;
				maxsgm = tempSgm[y][x];
			}
		}
	}
	
	printf("Max on SGM: %d\n", (int)maxsgm);
	for( y = 0; y < ROWS; y++)
	{
		for( x = 0; x < COLS; x++)
		{
			image[y][x] = (float)tempSgm[y][x]/(float)maxsgm * 255;
		}
	}
}

void edge_detection(unsigned char threshold, unsigned char	image[ROWS][COLS])
{
	
	int x, y;
	for(y = 0; y < ROWS; y++)
	{
		for(x = 0; x < COLS; x++)
		{
			if(image[y][x] > threshold)
				image[y][x] = 255;
			else
				image[y][x] = 0;
		}
	}
}



int main( int argc, char **argv )
{
	char ifile[20];
	char ofile[20];	
	unsigned char	image[ROWS][COLS];
	int imageDx[ROWS][COLS];
	int imageDy[ROWS][COLS];
	unsigned char imageSgm[ROWS][COLS];
	unsigned char threshold;
	int i;
	memset(imageDx, 0, ROWS * COLS * sizeof(int));
	memset(imageDy, 0, ROWS * COLS * sizeof(int));
	memset(imageSgm, 0, ROWS * COLS * sizeof(unsigned char));
	for(i = 1; i < 4; i++) {
		sprintf(ifile, "image%d", i);
		sprintf(ofile, "image%ddx.ras", i);
		read_image(ifile, image);
		/*x sobel*/
		x_sobel(image, imageDx);
		write_image(ofile, image);
		/*y_sobel*/
		read_image(ifile, image);
		y_sobel(image, imageDy);
		sprintf(ofile, "image%ddy.ras", i);
		write_image(ofile, image);
		/*sgm*/
		sprintf(ofile, "image%dsgm.ras", i);
		sgm(image, imageDx, imageDy, imageSgm);
		write_image(ofile, image);
		/*edge detection*/
		sprintf(ofile, "image%dedge.ras", i);
		switch(i){
			case 1:
				threshold = 80;
				break;
			case 2:
				threshold = 12;
				break;
			case 3:
				threshold = 25;
				break;
		}
		edge_detection(threshold, image);
		write_image(ofile, image);
	}

    return 0;
}



