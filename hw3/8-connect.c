/* 
	Let's call 
		x_c be the pixel at center,
		x_l be the pixel to the left,
		x_u be the upper pixel
		x_ul be the pixel at upper left corner
		x_ur be the pixel at upper right corner
		
	We'll have a table of equivalent label, called label_table
	Method add_label_table will add mark to label to be equivalent and store them in that table
*/
binary image[ROWS][COLS];
label_index = 0;
for (i = 0; i< ROWS; i++){
	for (j = 0; j < COLS; j++){
		
		/*they maybe out of bound, since this is pseudo code i won't check for that*/
		x_c = ROWS *i + j;
		x_l = ROWS *i + j - 1;
		x_u = ROWS * (i - 1) + j;
		x_ul = ROWS * (i - 1) + j -1 ;
		x_ur = ROWS * (i - 1) + j + 1 ;
		
		if (image[x_c] == 1) {
			if (image[x_l] == 0 and image[x_u] == 0 and image[x_ul] == 0 and image[x_ur] == 0) {
				label[x_c] = label_index;
				label_index++;
			}
			else if (image[x_l] == 0 and image[x_u] == 0 and image[x_ul] == 0 and image[x_ur] == 1) {
				label[x_c] = label[x_ur];
			}
			else if (image[x_l] == 0 and image[x_u] == 0 and image[x_ul] == 1 and image[x_ur] == 0) {
				label[x_c] = label[x_ul];
			}
			else if (image[x_l] == 0 and image[x_u] == 1 and image[x_ul] == 0 and image[x_ur] == 0) {
				label[x_c] = label[x_u];
			}
			else if (image[x_l] == 1 and image[x_u] == 0 and image[x_ul] == 0 and image[x_ur] == 0) {
				label[x_c] = label[x_l];
			}
			else if (image[x_l] == 0 and image[x_u] == 0 and image[x_ul] == 1 and image[x_ur] == 1) {
				label[x_c] = label[x_ul];
				add_label_table(label[x_ul], label[x_ur]
			}
			else if (image[x_l] == 0 and image[x_u] == 1 and image[x_ul] == 1 and image[x_ur] == 0) {
				label[x_c] = label[x_u];
				add_label_table(label[x_u], label[x_ul]);
			}
			else if (image[x_l] == 1 and image[x_u] == 1 and image[x_ul] == 0 and image[x_ur] == 0) {
				label[x_c] = label[x_l];
				add_label_table(label[x_l], label[x_u]);
			}
			else if (image[x_l] == 0 and image[x_u] == 1 and image[x_ul] == 0 and image[x_ur] == 1) {
				label[x_c] = label[x_u];
				add_label_table(label[x_u], label[x_ur]);
			}
			else if (image[x_l] == 1 and image[x_u] == 0 and image[x_ul] == 1 and image[x_ur] == 0) {
				label[x_c] = label[x_l];
				add_label_table(label[x_l], label[x_ul]);
			}
			else if (image[x_l] == 1 and image[x_u] == 0 and image[x_ul] == 0 and image[x_ur] == 1) {
				label[x_c] = label[x_l];
				add_label_table(label[x_l], label[x_ur]);
			}			
			else if (image[x_l] == 0 and image[x_u] == 1 and image[x_ul] == 1 and image[x_ur] == 1) {
				label[x_c] = label[x_u];
				add_label_table(label[x_u], label[x_ul], label[x_ur]);
			}
			else if (image[x_l] == 1 and image[x_u] == 1 and image[x_ul] == 1 and image[x_ur] == 0) {
				label[x_c] = label[x_l];
				add_label_table(label[x_l], label[x_u], label[x_ul]);
			}			
			else if (image[x_l] == 1 and image[x_u] == 1 and image[x_ul] == 0 and image[x_ur] == 1) {
				label[x_c] = label[x_l];
				add_label_table(label[x_l], label[x_u], label[x_ur]);
			}
			else if (image[x_l] == 1 and image[x_u] == 0 and image[x_ul] == 1 and image[x_ur] == 1) {
				label[x_c] = label[x_l];
				add_label_table(label[x_l], label[x_ul], label[x_ur]);
			}			
			else if (image[x_l] == 1 and image[x_u] == 1 and image[x_ul] == 1 and image[x_ur] == 1) {
				label[x_c] = label[x_l];
				add_label_table(label[x_l], label[x_u], label[x_ul], label[x_ur]);
			}
		}
	}
}