/*************************************************
 * EECS 101 Homework 3
 * Name: Quan Chau
 * Student ID: 18831829
 *************************************************/
 #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/*define number of NUM_IMG_ROWS and columns in image*/
#define NUM_IMG_ROWS   		 		 512
#define NUM_IMG_COLS				 512


void header( int row, int col, unsigned char head[32] )
{
	int *p = (int *)head;
	char *ch;
	int num = row * col;

	/* Choose little-endian or big-endian header depending on the machine. Don't modify this */
	/* Little-endian for PC */
	
	*p = 0x956aa659;
	*(p + 3) = 0x08000000;
	*(p + 5) = 0x01000000;
	*(p + 6) = 0x0;
	*(p + 7) = 0xf8000000;

	ch = (char*)&col;
	head[7] = *ch;
	ch ++; 
	head[6] = *ch;
	ch ++;
	head[5] = *ch;
	ch ++;
	head[4] = *ch;

	ch = (char*)&row;
	head[11] = *ch;
	ch ++; 
	head[10] = *ch;
	ch ++;
	head[9] = *ch;
	ch ++;
	head[8] = *ch;
	
	ch = (char*)&num;
	head[19] = *ch;
	ch ++; 
	head[18] = *ch;
	ch ++;
	head[17] = *ch;
	ch ++;
	head[16] = *ch;
	
/*
	// Big-endian for unix
	*p = 0x59a66a95;
	*(p + 1) = col;
	*(p + 2) = row;
	*(p + 3) = 0x8;
	*(p + 4) = num;
	*(p + 5) = 0x1;
	*(p + 6) = 0x0;
	*(p + 7) = 0xf8;
*/
}

void clear( unsigned char image[NUM_IMG_ROWS][NUM_IMG_COLS] )
{
	int	i,j;
	for ( i = 0 ; i < NUM_IMG_ROWS ; i++ )
		for ( j = 0 ; j < NUM_IMG_COLS ; j++ ) image[i][j] = 0;
}

		   
int main( int argc, char **argv )
{
	int				i, j, x, y, img_area, center_x, center_y;
	char ifile[20], ofile[20];
	unsigned char head[32];
	unsigned char	image[NUM_IMG_ROWS][NUM_IMG_COLS];
	
	for (i = 0; i < 3; i++)
	{
		sprintf(ifile, "image%d", i+1);
		sprintf(ofile, "image%d_after.ras",i+1);

		/*read in the file*/
		FILE * fh;
		if (( fh = fopen( ifile, "rb" )) == NULL )
		{
			fprintf( stderr, "error: couldn't open %s\n", ifile );
			exit( 1 );
		}
		
		for ( j = 0; j < NUM_IMG_ROWS ; j++ )
		{
			if ( fread( image[j], 1, NUM_IMG_COLS, fh ) != NUM_IMG_COLS )
			{
				fprintf( stderr, "error: couldn't read enough stuff\n" );
				exit( 1 );
			}
		}
		
		fclose(fh);
		
		/*these threshold values are from expiremnt, trial and error*/
		int threshold;
		switch(i){
			case 0: 
				threshold = 137;
				break;
			case 1:
				threshold = 165;
				break;
			case 2:
				threshold = 49;
				break;
		}
		
		/*use threshold to make image binary*/
		img_area = 0;
		center_x = 0;
		center_y = 0;
		for(x = 0; x < NUM_IMG_ROWS; x++)
		{
			for(y = 0; y < NUM_IMG_COLS; y++)
			{
				if(image[x][y] <= threshold)
				{
					img_area += 1;
					image[x][y] = 255;
					center_x += x;
					center_y += y;
				}
				else
				{
					image[x][y] = 0;
				}
			}
		}
		center_x /= img_area;
		center_y /= img_area;
		
		/*color the center with gray*/
		for(x = center_x - 2; x < center_x + 3; x++)
		{
			for(y = center_y - 2; y < center_y + 3; y++)
			{
				image[x][y] = 128;
			}
		}
		
		//print area and center
		printf ("We found the area of image%d = %d and center at(%d, %d)\n\n", (i+1), img_area , center_x, center_y);
		
		/* Create a header  to print out in ras format*/
		header(NUM_IMG_ROWS, NUM_IMG_COLS, head);
		
		/*Write result to file*/
		if (!( fh = fopen( ofile, "wb" )))
		{
			fprintf( stderr, "error: could not open %s\n", ofile ), exit(1);
		}
		fwrite( head, 4, 8, fh );

		for ( j = 0; j < NUM_IMG_ROWS; j++ )
		{
			fwrite( image[j], 1, NUM_IMG_COLS, fh );
		}

		fclose( fh );
		clear( image );
	}
	return 0;
}



