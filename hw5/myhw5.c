/****************
 * Name: Quan Chau
 * Student ID: 18831829
 * EECS 101 homework 5
 ****************/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RHO_VALUE_RANGE			2000 
#define ROWS   		 		 480
#define COLS				 640
#define NUM_TOP_LINES			4		

void header(int row, int col, unsigned char head[32])
{
	
   int *p = (int *)head;
    char *ch;
    int num = row * col;
    
    /* Choose little-endian or big-endian header depending on the machine. Don't modify this */
    /* Little-endian for PC */
    
    *p = 0x956aa659;
    *(p + 3) = 0x08000000;
    *(p + 5) = 0x01000000;
    *(p + 6) = 0x0;
    *(p + 7) = 0xf8000000;
    
    ch = (char*)&col;
    head[7] = *ch;
    ch ++; 
    head[6] = *ch;
    ch ++;
    head[5] = *ch;
    ch ++;
    head[4] = *ch;
    
    ch = (char*)&row;
    head[11] = *ch;
    ch ++; 
    head[10] = *ch;
    ch ++;
    head[9] = *ch;
    ch ++;
    head[8] = *ch;
    
    ch = (char*)&num;
    head[19] = *ch;
    ch ++; 
    head[18] = *ch;
    ch ++;
    head[17] = *ch;
    ch ++;
    head[16] = *ch;
    
    /*
     // Big-endian for unix
     *p = 0x59a66a95;

     *(p + 1) = col;
     *(p + 2) = row;
     *(p + 3) = 0x8;
     *(p + 4) = num;
     *(p + 5) = 0x1;

     *(p + 6) = 0x0;
     *(p + 7) = 0xf8;
     */
}

void read_image(char ifile[20], unsigned char	image[ROWS][COLS])
{
	int k; FILE * fp;
	if((fp = fopen( ifile, "rb")) == NULL)
	{
		fprintf( stderr, "error: couldn't open %s\n", ifile );
		exit( 1 );
	}
	
	/* Read the file */
	for (k = 0; k < ROWS ; k++)
	{
		if (fread( image[k], 1, COLS, fp ) != COLS)
		{
			fprintf( stderr, "error: couldn't read enough stuff\n" );
			exit( 1 );
		}
	}
	
	/* Close the file */
	fclose( fp );
}

void Clear( unsigned char image[ROWS][COLS])
{
    int	i,j;
    for ( i = 0 ; i < ROWS ; i++ )
        for ( j = 0 ; j < COLS ; j++ ) image[i][j] = 0;
}

void write_image(char ofile[20], unsigned char	image[ROWS][COLS])
{
	int k;
	unsigned char head[32];
	header(ROWS, COLS, head);
	FILE * fp;			
	if (!(fp = fopen( ofile, "wb")))
		fprintf( stderr, "error: could not open %s\n", ofile ), exit(1);
	
	fwrite( head, 4, 8, fp );
	
	for (k = 0; k < ROWS; k++)
		fwrite(image[k], 1, COLS, fp);
	
	fclose(fp);
}

void x_sobel(unsigned char	image[ROWS][COLS], int imageDx[ROWS][COLS])
{
	int sobelDx_matrix[3][3] = {{ -1,  0,  1 },
                    { -2,  0,  2 },
                    { -1,  0,  1 }};
	int first_time = 0;

	int y, x, k, xprime, max, l;
		
	for(y = 1; y < ROWS - 1; y++)
	{
		for(x = 1; x < COLS - 1; x++)
		{
			xprime = 0;
			for(l = 0; l < 3; l++)
				for(k = 0; k < 3; k++)
					xprime += image[k + y - 1][l + x - 1] * sobelDx_matrix[k][l];
			if(xprime < 0)
				xprime *= -1;
			if(xprime > max || first_time == 0){
				first_time = 1;
				max = xprime;
			}
			imageDx[y][x] = xprime;
		}
	}
	
	printf("Max on dx: %f\n", (float)max);
	for(y = 0; y < ROWS; y++)
	{
		for( x = 0; x < COLS; x ++){
			image[y][x] = (float)imageDx[y][x]/(float)max * 255.0;
		}
	}
}

void y_sobel(unsigned char	image[ROWS][COLS], int imageDy[ROWS][COLS])
{	
	int sobelDy_matrix[3][3] = {{ 1,  2,  1 },
                    { 0,  0,  0 },
                    { -1,  -2,  -1 }};
	int first_time = 0;
	int y, k,x, l, yprime, max;

	for(y = 1; y < ROWS - 1; y++)
	{
		for(x = 1; x < COLS - 1; x++)
		{
			yprime = 0;
			for(l = 0; l < 3; l++)
				for(k = 0; k < 3; k++)
					yprime += image[k + y - 1][l + x - 1] * sobelDy_matrix[k][l];
			if(yprime < 0)
				yprime *= -1;
			if(yprime > max || first_time == 0){
				max = yprime;
				first_time = 1;
			}
			imageDy[y][x] = yprime;
		}
	}
	
	printf("Max on dy: %f\n", (float)max);
	for(y = 0; y < ROWS; y++)
	{
		for( x = 0; x < COLS; x ++)
			image[y][x] = (float)imageDy[y][x]/(float)max * 255;
	}
}

void sgm(unsigned char	image[ROWS][COLS], int imageDx[ROWS][COLS], int imageDy[ROWS][COLS], unsigned char imageSgm[ROWS][COLS])
{
	int tempSgm[ROWS][COLS];
	int maxsgm = 0, y, x; int first_time = 0;
	for(y = 0; y < ROWS ; y++)
	{
		for(x = 0; x < COLS ; x++)
		{
			tempSgm[y][x] = ((int)imageDx[y][x]*imageDx[y][x])+((int)imageDy[y][x]*imageDy[y][x]);
			if(tempSgm[y][x] > maxsgm || first_time == 0){
				first_time = 1;
				maxsgm = tempSgm[y][x];
			}
		}
	}
	
	printf("Max on SGM: %d\n", (int)maxsgm);
	for( y = 0; y < ROWS; y++)
	{
		for( x = 0; x < COLS; x++)
		{
			image[y][x] = (float)tempSgm[y][x]/(float)maxsgm * 255;
		}
	}
}

void edge_detection(unsigned char threshold, unsigned char	image[ROWS][COLS])
{
	
	int x, y;
	for(y = 0; y < ROWS; y++)
	{
		for(x = 0; x < COLS; x++)
		{
			if(image[y][x] > threshold)
				image[y][x] = 255;
			else
				image[y][x] = 0;
		}
	}
}

// to find the result in radian
float calc_cos(int theta_degree)
{
	return sinf(theta_degree * 3.14159 / 180);
}

// to find the result in radian
float calc_sin(int theta_degree)
{
	return cosf(theta_degree * 3.14159 / 180);
}

//find out how long the range of rho is 
void find_rho(unsigned char thresholdImage[ROWS][COLS], int * rho_min_ret, int * rho_max_ret)
{
	int rho_min = 0;
	int rho_max = 0;
	int first_time = 1;
	int x,y, theta_degree, rho;	
	for(x = 0; x < COLS; x++)
	{
		for(y = 0; y < ROWS; y++)
		{
			if (thresholdImage[y][x] == 255) 
			{
				for (theta_degree = 0; theta_degree < 181; theta_degree++)
				{
					rho = (y * calc_cos(theta_degree)) - (x * calc_sin(theta_degree));
					if (first_time) {
						rho_max = rho;
						rho_min = rho;
						first_time = 0;
					}
					if (rho > rho_max) {rho_max = rho;}
					if (rho < rho_min) {rho_min = rho;}
				}
			}
		}
	}
	printf ("rho_min = %d\n", rho_min);
	printf ("rho_max = %d\n", rho_max);
	*rho_min_ret = rho_min;
	*rho_max_ret = rho_max;
}

//add up all similar theta and rho
void vote_accumulate(unsigned char thresholdImage[ROWS][COLS], int rho_min, unsigned int vote_sum[181][RHO_VALUE_RANGE])
{
	int x,y;
	int theta_degree, rho;
	for(y = 0; y < ROWS; y++)
	{
		for(x = 0; x < COLS; x++)
		{
			if (thresholdImage[y][x] == 255) 
			{
				for (theta_degree = 0; theta_degree < 181; theta_degree++)
				{
					rho = (x * calc_cos(theta_degree)) - (y * calc_sin(theta_degree));
					vote_sum[theta_degree][rho - rho_min]++;
				}
			}
		}
	}
}

//find top lines
void find_top_lines(unsigned int vote_sum[181][RHO_VALUE_RANGE], int rho_min, int rho_max, int rho_array[NUM_TOP_LINES], int theta_array[NUM_TOP_LINES])
{
	int theta_degree, rho,i;
	int count_find = 0;

	//for(i = 0; i < NUM_TOP_LINES; i++)
	//{
		for(theta_degree = 0; theta_degree < 181; theta_degree++)
		{
			for(rho = rho_min; rho < rho_max; rho++)
			{
				if (vote_sum[theta_degree][rho-rho_min] >= 180) 
				{	
					rho_array[count_find] = rho;
					theta_array[count_find] = theta_degree;
					printf("Line%d: Theta = %d   Rho = %d   number_of_votes = %d\n", count_find, theta_degree, rho, vote_sum[theta_degree][rho-rho_min]);
					count_find++;
				}
			}
		}
	//}
}


//draw all top lines 
void draw_lines(int rho_array[NUM_TOP_LINES], int theta_array[NUM_TOP_LINES], unsigned char	image[ROWS][COLS])
{
	int temp, theta_degree, rho, x, y, i;
	for (i = 0; i < NUM_TOP_LINES; i++){
		theta_degree = theta_array[i];
		rho = rho_array[i];

		for(y = 0; y < ROWS; y++)
		{
			for(x = 0; x < COLS; x++)
			{
				temp = (y * calc_sin(theta_degree)) - (x * calc_cos(theta_degree)) + rho ;
				if (temp == 0)
				{
					image[y][x] = 255;
				}
			}
		}
	}

}

int main( int argc, char **argv )
{
	char ifile[20];
	char ofile[20];	
	unsigned char	image[ROWS][COLS];
	int imageDx[ROWS][COLS];
	int imageDy[ROWS][COLS];
	unsigned int vote_sum[181][RHO_VALUE_RANGE];
	unsigned char imageSgm[ROWS][COLS];
	unsigned char threshold;
	int rho_min, rho_max;
	int select_rho[NUM_TOP_LINES], select_theta[NUM_TOP_LINES];
	memset(imageDx, 0, ROWS * COLS * sizeof(int));
	memset(imageDy, 0, ROWS * COLS * sizeof(int));
	memset(imageSgm, 0, ROWS * COLS * sizeof(unsigned char));
	memset(vote_sum, 0, 181 * RHO_VALUE_RANGE * sizeof(unsigned int));

	sprintf(ifile, "image");
	sprintf(ofile, "imagedx.ras");
	read_image(ifile, image);
	/*x sobel*/
	x_sobel(image, imageDx);
	write_image(ofile, image);
	/*y_sobel*/
	read_image(ifile, image);
	y_sobel(image, imageDy);
	sprintf(ofile, "imagedy.ras");
	write_image(ofile, image);
	/*sgm*/
	sprintf(ofile, "imagesgm.ras");
	sgm(image, imageDx, imageDy, imageSgm);
	write_image(ofile, image);
	/*edge detection*/
	sprintf(ofile, "imageedge.ras");
	threshold = 200;
	edge_detection(threshold, image);
	write_image(ofile, image);
	/*hw5 starts here*/
	find_rho(image, &rho_min, &rho_max); 
	vote_accumulate(image, rho_min, vote_sum);
	find_top_lines(vote_sum, rho_min, rho_max, select_rho, select_theta);
	memset(image, 0, ROWS * COLS * sizeof(unsigned char));
	draw_lines( select_rho, select_theta, image);
	sprintf(ofile, "imageredo.ras");
 	write_image(ofile, image);
	return 0;
}



