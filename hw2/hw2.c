/*************************************************
 * EECS 101 Homework 2
 * Name: Quan Chau
 * Student ID: 18831829
 *************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define ROWS   		 		 100
#define COLS				 100
#define LOGICAL_X_MIN			-4.0
#define LOGICAL_X_MAX			 4.0
#define LOGICAL_Y_MIN			-4.0
#define LOGICAL_Y_MAX			 4.0

float calculate_uHat(unsigned char image[ROWS][COLS]);
float calculate_sigma(unsigned char image[ROWS][COLS], float uHat);
void calculate_image(char *ifile, float * uHat, float * sigma);

int main( int argc, char **argv )
{
	char input_file[20];
	int i;
	float uHat, sigma;
	for (i = 1; i < 5; i++){
		sprintf(input_file, "image%d", i);
		calculate_image(input_file, &uHat, &sigma);
		printf("From file %s, uHat = %.2f, sigma = %.2f\n", input_file, uHat, sigma);
	}
	return 0;
}

void calculate_image(char *ifile, float * uHat, float * sigma)
{
	int				i, j;
	FILE			*fp;
	unsigned char	image[ROWS][COLS];
	/* Clear image buffer */
	memset (image, 0, sizeof(unsigned char) * ROWS * COLS);

	/* Read in a raw image */
	/* Open the file */
	if (( fp = fopen( ifile, "rb" )) == NULL )
	{
		fprintf( stderr, "error: couldn't open %s\n", ifile );
		exit( 1 );
	}			

	/* Read the file */
	for ( i = 0; i < ROWS ; i++ ){
		if ( fread( image[i], 1, COLS, fp ) != COLS )
		{
			fprintf( stderr, "error: couldn't read enough stuff\n" );
			exit( 1 );
		}
	}
	
	* uHat = calculate_uHat(image);
	* sigma = calculate_sigma(image, *uHat);
	
}



float calculate_uHat(unsigned char image[ROWS][COLS])
{
	float uHat = 0;
	int i, j;
	for ( j = 0; j < COLS; j++ )
	{
		for ( i = 0; i < ROWS; i++ )
		{
			uHat = uHat + image[i][j];
		}
	}
	uHat = uHat / (COLS * ROWS);
	return uHat;
}

float calculate_sigma(unsigned char image[ROWS][COLS], float uHat)
{
	float sigma = 0;
	int i, j;
	for ( i = 0; i < ROWS; i++ )
	{
		for ( j = 0; j < COLS; j++ )
		{
			sigma = sigma + ((image[i][j] - uHat) * (image[i][j] - uHat));
		}
	}
	sigma = sigma / ((COLS * ROWS) - 1);
	return sigma;
}
